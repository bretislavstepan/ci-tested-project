#!/usr/bin/env bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq

# Install git (the php image doesn't have it) which is required by composer
apt-get install git -yqq

# Install zip
apt-get install zip unzip -yqq
