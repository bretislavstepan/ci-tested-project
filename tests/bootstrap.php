<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

// 1. create test database
// 2. load test data
// or copy from production .sql

$configurator->setTempDirectory(__DIR__ . '/../temp');

$databaseName = getenv('database');

$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/../app/config/config.test.neon');



return $configurator->createContainer();

