<?php declare(strict_types=1);

namespace App\Tests\Forms;

use App\Forms\FormFactory;
use PHPUnit\Framework\TestCase;

final class FormFactoryTest extends TestCase
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    protected function setUp()
    {
        $this->formFactory = $this->createFormFactory();
    }

    public function testCreate()
    {
        // https://phpunit.de/manual/current/en/test-doubles.html#test-doubles.mock-objects.examples.SubjectTest.php
        $this->assertSame(5, $this->formFactory->getUserId());

        $form = $this->formFactory->create();
        // Form::class - PHP 5.5+
        // "Nette\Application\UI\Form" - PHP 5.4-
        $this->assertInstanceOf(
            'Nette\Application\UI\Form',
            $form
        );
    }

    /**
     * @group database1
     */
    public function testCount()
    {
        return $this->formFactory ;
    }

    /**
     * @return FormFactory
     */
    private function createFormFactory()
    {
        return new FormFactory($this->createDateProviderMock());
    }

    /**
     * @return \PHPUnit_Framework_MockObject_Builder_InvocationMocker
     */
    private function createDateProviderMock()
    {
        $userMock =  $this->createMock('Nette\Security\User');
        $userMock->method('getId')
            ->willReturn(5);

        return $userMock;
    }
}